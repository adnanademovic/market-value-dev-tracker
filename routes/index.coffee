express = require 'express'
assert = require 'assert'
database = require '../database'
router = express.Router()

# GET home page.
router.get '/', (req, res, next) ->
  if req.query.player?
    await database.Player.findOne
      name: req.query.player
    , defer err, entry
    unless entry
      res.redirect '/'
      return

    assert.equal err, null
    console.log entry.values
    res.render 'index-player',
      category: 'home'
      hostname: process.env.HOSTNAME
      title: 'Player stats'
      name: entry.name
      url: entry.url
      values: entry.values
  else
    await database.Player.find {},
      _id: false
      name: true
    ,
      sort:
        name: 1
    , defer err, entries

    assert.equal err, null
    console.log process.env.HOSTNAME
    res.render 'index',
      category: 'home'
      hostname: process.env.HOSTNAME
      title: 'Market Value Development Tracker'
      players: entries

module.exports = router
