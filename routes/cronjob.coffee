express = require 'express'
jsdom = require 'node-jsdom'
request = require 'request'
assert = require 'assert'
database = require '../database'
router = express.Router()

cronTick = ->
  getPlayers = (callback) ->
    await jsdom.env
      url: "http://www.transfermarkt.co.uk/jumplist/startseite/verein/11"
      scripts: ["http://code.jquery.com/jquery.js"]
      features:
        FetchExternalResources: ['script']
        ProcessExternalResources: ['script']
        MutationEvents: '2.0'
      headers: # The page is blocking bot requests, so I'm using a browser user agent.
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36'
      done: defer err, window
    $ = window.$
    players = {}
    for item in $('table.items').first().find("span.hide-for-small")
      hyperlink = $(item).find("a.spielprofil_tooltip").first()
      unless hyperlink.length isnt 1
        players[$(hyperlink).text()] = $(hyperlink).attr('href')
    callback players
  getPlayer = (name, url, callback) ->
    await request
      url: "http://www.transfermarkt.co.uk#{url}"
      headers: # The page is blocking bot requests, so I'm using a browser user agent.
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36'
    , defer err, response, body
    assert.equal err, null
    startPoint = body.search(/Highcharts\.Chart\(/g)
    body = body.substring(startPoint)
    startPoint = body.search(/'series':/g)
    body = body.substring(startPoint)
    startPoint = body.search(/\{/g)
    currentPoint = startPoint
    while body[currentPoint++] isnt '{'
      null
    bracketCounter = 1
    while bracketCounter > 0
      if body[currentPoint] is '{'
        bracketCounter++
      else if body[currentPoint] is '}'
        bracketCounter--
      currentPoint++
    reading = body.substring startPoint, currentPoint
    reading = reading.replace /'/g, '\"'
    reading = JSON.parse reading
    callback
      $setOnInsert:
        name: name
        url: url
      $push:
        values:
          $each:
            for value in reading['data']
              time: new Date value['x']
              pounds: value['y']
  await getPlayers defer players
  console.log players
  for name, url of players
    await getPlayer name, url, defer doc
    conditions =
      name: name
    options =
      upsert: true
    database.Player.update conditions, doc, options, (err, raw) ->
      assert.equal err, null

  console.log "Fetched update from Player stats."

cleanDatabase = ->
  o =
    map: ->
      times = this.values.sort (a, b) ->
        return a.time - b.time
      hasCollision = false
      for i in [1...times.length]
        if times[i - 1].time.getTime() is times[i].time.getTime()
          hasCollision = true
          break
      unless hasCollision
        return
      emit this._id, times[0]
      for i in [1...times.length]
        if times[i - 1].time.getTime() isnt times[i].time.getTime()
          emit this._id, times[i]
    reduce: (key, value) ->
      return JSON.stringify value
  await database.Player.mapReduce o, defer err, results
  for item in results
    value = JSON.parse item.value
    unless value.length?
      value = [value]
    doc =
      $set:
        values: value
    conditions =
      _id: item._id
    database.Player.update conditions, doc, {}, (err, raw) ->
      assert.equal err, null

# GET home page.
router.get '/', (req, res, next) ->
  res.render 'cronjob',
    category: 'cronjob'
    hostname: process.env.HOSTNAME
    title: 'Fetcher Admin'
    baseUrl: req.baseUrl

# POST start cron.
router.post '/fetch', (req, res, next) ->
  cronTick()
  res.redirect '.'

# POST start cron.
router.post '/clean', (req, res, next) ->
  cleanDatabase()
  res.redirect '.'

# POST start cron.
router.post '/fetchclean', (req, res, next) ->
  cronTick()
  cleanDatabase()
  res.redirect '.'

module.exports = router
