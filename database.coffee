mongoose = require 'mongoose'

connectionString = '127.0.0.1:27017/marketvaluedevtracker'

if process.env.DB_PORT_27017_TCP_ADDR?
  connectionString = process.env.DB_PORT_27017_TCP_ADDR + ':' +
  process.env.DB_PORT_27017_TCP_PORT + '/marketvaluedevtracker'

mongoUrl = 'mongodb://' + connectionString

console.log 'Connecting to MongoDB server...'

mongoose.connect mongoUrl, {}, ->
  console.log 'Connected to MongoDB server.'

playerSchema = new mongoose.Schema
  name: String
  url: String
  values: [
    _id: false
    time: Date
    pounds: Number
  ]

module.exports =
  Player: mongoose.model 'Player', playerSchema
